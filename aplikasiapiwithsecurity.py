from flask import Flask, request, jsonify
import jwt
import datetime
from functools import wraps
import requests

app = Flask(__name__)
app.config["SECRET_KEY"] = "@(*)#$iowpmcieupONPOI09834"

def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = request.headers.get("JWT")
        if not token:
            return jsonify({"data":"Token Diperlukan", "code": 403}), 403        
        try:
            data = jwt.decode(token, app.config["SECRET_KEY"])
            print(data)
            # return data
        except:
            return jsonify({"data":"Token Invalid", "code": 403}), 403
        return f(*args, **kwargs)
    return decorator


@app.route('/', methods=['GET'])
# @token_required
def index():
    data = requests.get('https://data.bmkg.go.id/DataMKG/TEWS/autogempa.json').json()['Infogempa']['gempa']
    return jsonify(data)

@app.route("/login", methods=["GET","POST"])
def login():
    try:
        data = request.json
        if (data['username'] == "admin" and data['password'] == "admin123"):
            token = jwt.encode({
                "data": data['username'],
                "ip": request.remote_addr,
                "exp": datetime.datetime.utcnow()+datetime.timedelta(minutes=30)
            }, app.config["SECRET_KEY"])
            return jsonify({"data": token.decode("UTF-8") , "code": 200}), 200            
        else:
            return jsonify({"data":"Login Gagal", "code": 403}), 403
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if __name__ == '__main__':
    app.run(debug=True, port=5001)